#เริ่มต้นใช้งาน
#### การติดตั้ง

1. `cd Test-myapp` เพื่อเข้าไปในรูทโปรเจ็กต์
1. `yarn install` เพื่อติดตั้ง npm ดีเพนเดนซี (npm dependencie) ของเว็บไซต์

#### การทำงาน
1. `yarn start` เพื่อที่จะเริ่มต้นฮอทรีโหลดดิ้ง (hot-reloading)
1. `open http://localhost:3000`