import CardComponent from "./component/CardComponent";
import SelectDropdown from "./component/Dropdown";
import SearchInput from "./component/SearchInput";
import Navbar from "./component/Navbar";
import React from "react";
import { makeStyles } from "@material-ui/core/styles";
import { useState, useEffect } from "react";
import Button from "@material-ui/core/Button";
import { Box } from "@material-ui/core";
import Grid from "@material-ui/core/Grid";

const useStyles = makeStyles((theme) => ({
  container: {
    backgroundColor: "#E0E0E0",
  },
  paper: {
    padding: "20px",
    paddingTop: "80px",
    display: "flex",
    alignItems: "center",
    justifyContent: "end",
    position: "relative",
  },
  gridContainer: {
    display: "flex",
    flexWrap: "wrap",
    paddingLeft: "100px",
    justifyContent: "center",
  },
  buttonContainer: {
    display: "flex",
    justifyContent: "center",
  },
  button: {
    margin: "10px",
    justifyContent: "center",
    borderRadius: 50,
    width: "50px",
    height: "50px",
  },
}));

function App() {
  const classes = useStyles();
  const [name, setName] = useState([]);
  const [word, setWord] = useState("");
  const [dataFilter] = useState(["name"]);
  const [dataInPage, setDataInPage] = useState([]);
  const [page, setPage] = useState(0);
  const [data] = useState(["categories"]);
  const [typefood, setTypeFood] = useState([]);
  const [pageSize, setPageSize] = React.useState(5);

  useEffect(() => {
    fetch("example_data.json")
      .then((res) => res.json())
      .then((data) => {
        setName(data);
      });
  }, []);

  const searchKeyword = (name) => {
    return name.filter((item) => {
      return dataFilter.some((filter) => {
        return (
          item[filter].toString().toLowerCase().indexOf(word.toLowerCase()) > -1
        );
      });
    });
  };

  const changeTypeFood = (e) => {
    const category = e.target.value;
    if (category === " ") {
      setTypeFood(name);
    } else {
      const result = data.filter((element) => {
        return element.categories === category;
      });
      setTypeFood(result);
    }
  };

  const pagination = () => {
    const foodPerPage = 9;
    const pages = Math.ceil(name.length / foodPerPage);
    const newFood = Array.from({ length: pages }, (data, index) => {
      const start = index * foodPerPage;
      return name.slice(start, start + foodPerPage);
    });
    return newFood;
  };

  const handlePage = (index) => {
    setPage(index);
  };
  useEffect(() => {
    const paginate = pagination();
    setDataInPage(paginate);
  }, []);

  return (
    <Box>
      <Box className={classes.container}>
        <Navbar />

        <Grid container spacing={5} className={classes.paper}>
          <Grid item xs={2}>
            <SelectDropdown onChange={changeTypeFood} />
          </Grid>

          <Grid item xs={4}>
            <SearchInput
              value={word}
              onChange={(e) => setWord(e.target.value)}
            />
          </Grid>
        </Grid>

        <Box className={classes.gridContainer}>
          {searchKeyword(name).map((item, index) => (
            <Box key={index}>
              <CardComponent
                profileimage={item.profile_image_url}
                title={item.name}
                imgList={item.images}
                rating={item.rating}
              />
            </Box>
          ))}
        </Box>
        <Box className={classes.buttonContainer}>
          {dataInPage.map((data, index) => {
            return (
              <Button
                className={classes.button}
                key={index}
                variant="contained"
                color="primary"
                onClick={() => handlePage(index)}
              >
                {index + 1}
              </Button>
            );
          })}
        </Box>
      </Box>
    </Box>
  );
}

export default App;
