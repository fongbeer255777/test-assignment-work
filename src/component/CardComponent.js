import React from 'react';
import { makeStyles } from '@material-ui/core/styles';
import clsx from 'clsx';
import Card from '@material-ui/core/Card';
import CardHeader from '@material-ui/core/CardHeader';
import CardMedia from '@material-ui/core/CardMedia';
import CardContent from '@material-ui/core/CardContent';
import CardActions from '@material-ui/core/CardActions';
import Collapse from '@material-ui/core/Collapse';
import Avatar from '@material-ui/core/Avatar';
import IconButton from '@material-ui/core/IconButton';
import Typography from '@material-ui/core/Typography';
import { red } from '@material-ui/core/colors';
import FavoriteIcon from '@material-ui/icons/Favorite';
import ShareIcon from '@material-ui/icons/Share';
import ExpandMoreIcon from '@material-ui/icons/ExpandMore';
import MoreVertIcon from '@material-ui/icons/MoreVert';
import Brightness1Icon from '@material-ui/icons/Brightness1';
import { Box } from "@material-ui/core";
const useStyles = makeStyles((theme) => ({
  root: {
    maxWidth: 370,
    margin: "10px",
    borderRadius: 15,
  },
  media: {
    height: 0,
    paddingTop: '56.25%', // 16:9
  },
  expand: {
    transform: 'rotate(0deg)',
    marginLeft: 'auto',
    transition: theme.transitions.create('transform', {
      duration: theme.transitions.duration.shortest,
    }),
  },
  expandOpen: {
    transform: 'rotate(180deg)',
  },
  avatar: {
    height: "50px",
    width: "50px",
  },
  title: {
    fontFamily: "Kanit",
    fontWeight: "bold",
  },
  subtitle: {
    fontFamily: "Kanit",
    fontWeight: 400,
  },
  iconBut: {
    width: "10px",
    color: "#OF1E56",
  },
  images: {
    height: "120px",
    width: "117px",
  },
  images1: {
    flexGrow: 0,
    flexBasis: "100%",
    display: 'flex',
    margin: '10px',
  },
}));

export default function CardComponent({ profileimage, title, imgList = [] ,rating}) {
  const classes = useStyles();
  const [expanded, setExpanded] = React.useState(false);

  const handleExpandClick = () => {
    setExpanded(!expanded);
  };

  return (
    <Card className={classes.root}>
      <CardHeader
        avatar={
          <Avatar variant="rounded" className={classes.avatar}>
                <img className={classes.avatar} src={profileimage} />
              </Avatar>
        }
        action={
          <IconButton >
            <Brightness1Icon className={classes.iconBut}/>
            <Typography className={classes.title} variant="body1">
            {rating}
          </Typography>
          </IconButton>
        }
        title={
          <Typography className={classes.title} variant="body1">
            {title}
          </Typography>
        }
        subheader={
          <Typography className={classes.subtitle} variant="body1">
            {/* {item.operation_time.map((data,index) => (
                          <div key={index}>{data[1].day}</div>
                        ))} */}
            10:00 AM - 6:00 PM
          </Typography>
        }
        
      />
   
   <Box container className={classes.images1}>
              {imgList.map((itemImg, value) => (
                <Box key={value} item xs={4}>
                  <img className={classes.images} src={itemImg} />
                </Box>
              ))}
            </Box>
    
    
    </Card>
  );
}