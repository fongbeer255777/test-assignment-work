import React from "react";
import { makeStyles, withStyles } from "@material-ui/core/styles";
import FormControl from "@material-ui/core/FormControl";
import Select from "@material-ui/core/Select";
import InputBase from "@material-ui/core/InputBase";

const BootstrapInput = withStyles((theme) => ({
  input: {
    borderRadius: 30,
    position: "relative",
    backgroundColor: theme.palette.background.paper,
    border: "1px solid #134B84",
    padding: "10px 26px 10px 12px",
    transition: theme.transitions.create(["border-color", "box-shadow"]),
    width: "120px",
    color: "#605C5C",
    fontFamily: "Kanit",
    fontWeight: "wght@100",
    fontSize: "14px",
  },
}))(InputBase);

const useStyles = makeStyles((theme) => ({
  form: {
    margin: theme.spacing(1),
  },
  item: {
    color: "#605C5C",
    fontFamily: "Kanit",
    fontWeight: "wght@100",
    fontSize: "14px",
  },
}));

const SelectDropdown=({changeTypeFood})=>{
  const classes = useStyles();

  return (
      <FormControl className={classes.form}>
        <Select
          labelId="demo-customized-select-label"
          id="demo-customized-select"
          input={<BootstrapInput />}
          onChange={changeTypeFood}
        >
           <option className={classes.item} aria-label="None" value=" "></option>
          <option className={classes.item} value="restaurant">
            restaurant
          </option>
          <option className={classes.item} value="bakery">
            bakery
          </option>
          <option className={classes.item} value="cafe">
            cafe
          </option>
        </Select>
      </FormControl>
   
  );
}
export default SelectDropdown