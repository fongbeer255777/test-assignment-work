import React from "react";
import { makeStyles } from "@material-ui/core/styles";
import MenuIcon from "@material-ui/icons/Menu";
import AccountCircle from "@material-ui/icons/AccountCircle";
import Badge from "@material-ui/core/Badge";
import NotificationsIcon from "@material-ui/icons/Notifications";
import ExpandMoreIcon from "@material-ui/icons/ExpandMore";
import InboxIcon from "@material-ui/icons/MoveToInbox";
import {
  ListItemText,
  Drawer,
  ListItem,
  List,
  Divider,
  CssBaseline,
  IconButton,
  Typography,
  AppBar,
  Toolbar,
} from "@material-ui/core";

const drawerWidth = 70;

const useStyles = makeStyles((theme) => ({
  root: {
    display: "flex",
  },
  appBar: {
    width: `calc(100% - ${drawerWidth}px)`,
    marginLeft: drawerWidth,
  },
  menuButton: {
    fontSize: "16px",
    color: "#fff",
  },
  hide: {
    display: "none",
  },
  drawer: {
    width: drawerWidth,
    flexShrink: 0,
  },
  drawerPaper: {
    width: drawerWidth,
    boxShadow: "5px 10px 10px rgba(0, 0, 0, 0.2)",
    borderTopRightRadius: 100,
  },
  toolbar: theme.mixins.toolbar,

  content: {
    flexGrow: 1,
    backgroundColor: theme.palette.background.default,
    padding: theme.spacing(3),
  },
  listbar: {
    display: "block",
  },
  title: {
    flexGrow: 1,
    fontSize: "18px",
  },
  accountCircle: {
    width: "40px",
    height: "40px",
    Color: "#FFFF",
  },
  bar: {
    backgroundColor: "#134B84",
  },
  SideBarFont: {
    fontSize: "14px",
    display: "flex",
    alignItems: "center",
  },
  icon: {
    width: "30px",
  },
}));

export default function Dashboard(props) {
  const classes = useStyles();

  return (
    <div className={classes.root}>
      <CssBaseline />
      <AppBar position="fixed" className={classes.bar}>
        <Toolbar>
          <IconButton
            edge="start"
            className={classes.menuButton}
            color="inherit"
            aria-label="menu"
          >
            <MenuIcon />
          </IconButton>
          <Typography variant="h6" className={classes.title}></Typography>
          <IconButton color="inherit">
            <Badge
              color="secondary"
              overlap="circular"
              badgeContent=" "
              variant="dot"
            >
              <NotificationsIcon />
            </Badge>
          </IconButton>
          <IconButton
            aria-label="account of current user"
            aria-controls="menu-appbar"
            aria-haspopup="true"
            color="inherit"
          >
            <AccountCircle className={classes.accountCircle} />
          </IconButton>
          <IconButton color="inherit" aria-label="show more">
            <ExpandMoreIcon />
          </IconButton>
        </Toolbar>
      </AppBar>
      <Drawer
        className={classes.drawer}
        variant="permanent"
        anchor="left"
        classes={{
          paper: classes.drawerPaper,
        }}
      >
        <div className={classes.toolbar} />

        <Divider />
        <List component="nav" aria-label="main mailbox folders">
          <ListItem button className={classes.listbar}>
            <InboxIcon className={classes.icon} />

            <ListItemText
              disableTypography
              className={classes.SideBarFont}
              primary="Place"
            />
          </ListItem>
        </List>
        <Divider />
      </Drawer>
    </div>
  );
}
