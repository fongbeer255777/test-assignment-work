import React from "react";
import { makeStyles } from "@material-ui/core/styles";
import Card from "@material-ui/core/Card";
import InputBase from "@material-ui/core/InputBase";
import IconButton from "@material-ui/core/IconButton";
import SearchIcon from "@material-ui/icons/Search";


const useStyles = makeStyles((theme) => ({
  root: {
    padding: "2px 4px",
    display: "flex",
    alignItems: "center",
    width: 400,
    borderRadius: 30,
  },
  input: {
    color: "#605C5C",
    fontFamily: "Kanit",
    fontWeight: "wght@200",
    fontSize: "14px",
    marginLeft: theme.spacing(1),
    flex: 1,
  },
  iconButton: {
    padding: 10,
  },
}));

export default function SearchInput({value,onChange}) {
  const classes = useStyles();

  return (
    <Card className={classes.root}>
      <InputBase className={classes.input} placeholder="Search name..." value={value} onChange={onChange}/>
      <IconButton
        type="submit"
        className={classes.iconButton}
        aria-label="search"
      >
        <SearchIcon />
      </IconButton>
    </Card>
  );
}
